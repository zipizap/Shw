
module Shw
  # :) inspired by https://zipizap.wordpress.com/2011/09/28/quick-bash-colors/
  GRAY   = "[1;30m"
  WHITE  = "[37m"
  WHITY  = "[1;37m"
  GREEN  = "[1;32m"
  BLUE   = "[1;34m"
  YELLOW = "[1;33m" 
  RED    = "[1;31m"

  def self.gray(msg); self.puts(msg,GRAY);end
  def self.norm(msg); self.puts(msg,WHITE);end
  def self.white(msg); self.puts(msg,WHITY);end
  def self.green(msg); self.puts(msg,GREEN);end
  def self.blue(msg); self.puts(msg,BLUE);end
  def self.yellow(msg); self.puts(msg,YELLOW);end
  def self.red(msg); self.puts(msg,RED);end

  private
  def self.puts(msg,color_string)
    puts("\e"+color_string+msg+"\e[0m")
  end
end
